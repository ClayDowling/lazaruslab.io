---
title: "Home"
menu: main
weight: 1
---

A site for learning about electronics and software, for people who want to build things.
