---
title: "Starting With C"
date: 2022-01-21T21:19:57-05:00
draft: true
tags:
- programming
categories:
- beginner
image:  //via.placeholder.com/640x150
---

If you want to do embedded programming, you'll need to learn C or C++.  You're
also going to need tools.  Fortunately, all of the tools are free, and the
world is full of high quality free tools these days.

## Package Manager

The first tool you want to install is some variety of package manager.  If you
are working on Linux, your computer already has a package manager somewhere.
If you're on a Mac, I strongly recommend [Homebrew](https://brew.sh/).  For
windows users, you want [Chocolatey](https://chocolatey.org/).  Installing and
using a package manager will require administrative rights on your computer.
If you're on a corporate computer with a particularly restrictive security
policy, you may need to resort to blackmail or intimidation.  Blackmail
requires some preparation, so I've found intimidation to be more effective in
these cases.

## Compiler

The compiler is the C specific tool on the list.  The good news is that they're
free and easy to install.  For your operating system:

### Linux

    apt install build-essentials

### OSX

Type `cc --version` on the command line and see what happens.  If it responds
with some information about the compiler and a version number, it's already
installed.  If it's not installed, it will respond with instructions for
installing it.

### Windows

There are multiple C compilers for Windows.  They're all good, but one that
will be easiest to use is gcc, because it will use the same commands and
options as the compilers for Linux and OSX.  On Windows the gcc compiler is
part of a package called *mingw64.*  From an administrative powershell:

    choco install mingw64

## Editor

When you are starting out I strongly recommend that you don't use an IDE.
They're wonderful tools, and I pay an annual subscription fee for an IDE.  But
they add complexity, and when you're just starting out C is complex enough.

The easiest to use is [VS Code](https://code.visualstudio.com/).  This is 100%
not Visual Studio.  There's a good chance your package manager has this, but
it's easy to install from the website.  There is a recommended C/C++ bundle.
This bundle comes highly recommended, even though you will not use many of the
tools right away.  You probably don't need to install that bundle right away:
VS Code will prompt you to install the bundle the first time you try to edit C
code.

## Build Manager

The process of getting a collection of C files from source code to an assembled
program that can run on your computer takes a lot of steps.  You absolutely can
manage these manually by issuing the right commands at the command line.
You'll have every second of it.

To avoid that unfortunate fate, we use a build manager.  There are several
build management programs out there, but far and away the most popular is
*make,* which intimidates a lot of people.  Indeed, if you learn C at a college
or university, there's a better than even chance that your professor didn't
understand make.  Don't worry, there's whole articles on this site about using
make, with the goal of keeping it simple.

If you are using Windows you will need to install this (it's part of the
compiler tools installed with the other packages).  From an administrative
shell:

    choco install make

## Textbook

The classic way to learn C is via [The C Programming
Language](https://amzn.to/3qNoK2c).  It's ridiculously expensive (it wasn't
when I learned the language), but it is an excellent book.  It's a shockingly
concise introduction to a lot of advanced topics in computing.

Free Code Camp has a short online course for introducing you to C, [The C
Beginner's
Handbook](https://www.freecodecamp.org/news/the-c-beginners-handbook/) which
covers the basics.

# Summary

These are the basic tools that you'll need to learn and write C programs. Once
you have them, it's a fine opportunity to start working your way through the
book.
