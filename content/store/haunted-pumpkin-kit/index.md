---
title: "Haunted Pumpkin Kit"
description: "Parts kit for the Haunted Pumpkin Workshop"
date: 2021-05-23
images:
- product1.jpg
- product2.jpg
productid: haunted-pumpkin-kit
price: 15.00
weight: 5
categories:
- Product
---

The Haunted Pumpkin kit contains all of the electrical components necessary to
build your own blinking, glowing eyes.
