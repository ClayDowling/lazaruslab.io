---
title: "{{ replace .Name "-" " " | title }}"
description: ""
date: {{ now.Format "2006-01-02" }}
images:
- product1.jpg
- product2.jpg
productid: {{ .Name }}
price: 1.42
weight: 5
categories:
- Product
- Workshop
---
